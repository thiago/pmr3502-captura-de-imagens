#!/usr/bin/env python3
'''
	Author: Thiago Martins.
	"Inspirado" pelo servidor mjpeg feito por Igor Maculan
'''
import cv2
import logging
import os
import aiohttp
import asyncio
from aiohttp import web, MultipartWriter
import sys
import tempfile
import subprocess
import getopt
from threading import Condition
import io
from picamera2 import Picamera2
from picamera2.encoders import JpegEncoder
from picamera2.outputs import FileOutput
import os

# Página principal
#   Só uma tag <img> com o endereço do serviço mjpeg
#   e um botão que envia um comando websocket
pag_template = r"""<!DOCTYPE HTML>
<html>
	<head>
		<title>Captura de video</title>
		<meta charset="utf-8">
	</head>
	<body>
        <center>
            <div><img src="/image" /></div>
            <div><button id="botao_capturar" type="button">Capturar Imagem</button></div>
        </center>
		<script type="text/javascript">
var wsUri = (window.location.protocol=='https:'&&'wss://'||'ws://')+window.location.host+"/wsctrl";
var control_link = new WebSocket(wsUri);
document.getElementById("botao_capturar").onclick = function() {{envia_comando_capturar()}};
function envia_comando_capturar(){{
    control_link.send("capturar");
}}
		</script>
	</body>
</html>
"""

class StreamingOutput(io.BufferedIOBase):
    def __init__(self):
        self.frame = None
        self.condition = Condition()

    def write(self, buf):
        with self.condition:
            self.frame = buf
            self.condition.notify_all()


# Processa uma requisiçao mjpeg
#   Esta função consume os frames
#   escritos em app['last_frame']
#   FIXME: Essa arquitetura faz com que o
#       serviço fique "preso" a um cliente
#       e só possa encerrar depois que a conexão
#       for fechada
async def mjpeg_request(request):
    my_boundary = 'image-boundary'
    response = web.StreamResponse(
        status=200,
        reason='OK',
        headers={
            'Content-Type': 'multipart/x-mixed-replace;boundary={}'.format(my_boundary)
        }
    )
    await response.prepare(request)
    output = request.app['output']
    while True:
        await asyncio.sleep(0)
        with output.condition:
            output.condition.wait()
            frame = output.frame
            request.app['last_frame'] = frame
        with MultipartWriter('image/jpeg', boundary=my_boundary) as mpwriter:
            mpwriter.append(frame, {
                'Content-Type': 'image/jpeg'
            })
            try:
                await mpwriter.write(response, close_boundary=False)
            except ConnectionResetError :
                # Desconexão
                break
        await response.write(b"\r\n")

# Responde a uma solicitação de gravação
async def websocket_handler(request):
    ws = web.WebSocketResponse()
    diretorio = request.app['capt_path']
    await ws.prepare(request)
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            #grava o último quadro
            frame = request.app['last_frame']
            if frame is None:
                return
            with tempfile.NamedTemporaryFile(suffix=".jpg", prefix ="captura", delete=False, dir=diretorio) as file:
                print("Capturando em " + file.name)
                file.write(frame)

        elif msg.type == aiohttp.WSMsgType.ERROR:
            print('conexão ws encerrada com erro %s' %
                  ws.exception())
    print('conexão ws encerrada')
    return ws

async def root_handler(request):
    return aiohttp.web.Response(text=request.app['root_pag'], content_type="text/html")

async def inicializa_tarefas(app):
    app['camera'].start_recording(JpegEncoder(), FileOutput(app['output']))

async def encerra_tarefas(app):
    print("Shutdown")
    app['camera'].stop_recording()

def main():
    porta_servico = "8080"
    endereco_servidor = None
    diretorio_capturas = "./capturas"
    try:
      opts, args = getopt.getopt(sys.argv[1:],"h:pe:")
    except getopt.GetoptError:
      print('camera_stream.py -d <diretório de captura> -p <porta do servidor mjpeg> -e <endereco externo do servidor>')
      sys.exit(2)
    for opt, arg in opts:
        print(opt)
        if opt == '-h':
            print('camera_stream.py -p <porta do servidor mjpeg> -e <endereco externo do servidor>')
            sys.exit()
        elif opt in ("-p",):
            porta_servico = arg
        elif opt in ("-e",):
            endereco_servidor = arg
        elif opt in ("-d",):
            diretorio_capturas = arg

    if endereco_servidor == None:
        endereco_servidor = "0.0.0.0"

    print("Endereço do servidor mjpeg: " + endereco_servidor)
    print("Porta do servidor mjpeg: " + porta_servico)
    print("Diretório para imagens capturadas: " + diretorio_capturas)

    # Verifica se o diretório existe, caso não exista, cria
    if not os.path.isdir(diretorio_capturas):
        print("Diretório para imagens capturadas não existe, criando.")
        try:
            os.mkdir(diretorio_capturas)
        except:
            print("Impossível criar o caminho " + diretorio_capturas + ". Encerrando.")
            return -1


    app = web.Application()
    app['mjpeg_connections'] = {}
    app['root_pag'] = pag_template
    app['last_frame'] = None
    app['capt_path'] = diretorio_capturas
    picam2 = Picamera2(tuning=os.environ.get('LIBCAMERA_RPI_TUNING_FILE', None))
    picam2.configure(picam2.create_video_configuration(main={"size": (1296, 972)}))
    picam2.start(show_preview = False)
    app['camera'] = picam2;
    app['output'] = StreamingOutput()

    app.on_startup.append(inicializa_tarefas)
    app.on_cleanup.append(encerra_tarefas)
    # Página raiz
    app.router.add_route("GET", "/", root_handler)
    # Fluxo mjpeg
    app.add_routes([web.get('/image', mjpeg_request)])
    # Comando para gravar
    app.router.add_routes([web.get('/wsctrl', websocket_handler)])
    web.run_app(app, host=endereco_servidor, port=int(porta_servico), shutdown_timeout=0.2)

    return 0


if __name__ == '__main__':
    sys.exit(main())

